An assignment submission for Project Development Lab INFO2300 by Swathi Korrapatti 8667968
This includes a javascript file that I wrote for adding to this repository

To run this program.

1. Install node.js in your local system
2. Clone this repository
3. Open command prompt and navigate to the working directory
4. Type node test.js to run the code.

Also added code from last semester's Web design and development class

To run that project 
1. Follow the same steps of installing Node.js
2. Clone the repository
3. In the command prompt type npm install package.json
4. Run the code by typing node index.js
5. Open the browser to view the website.



## LICENSE ##

MIT License

Copyright (c)2020 SWATHI KORRAPATTI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


